// Ouverture App
describe ('Ouverture du localhost', function () {
  it ("Ouverture de l'application create-react-app en local sur le port 3000", function () {
    cy.visit ('baseUrl');
    cy.get ('.App-header').contains ('Edit src/App.js and save to reload.');
    // click the button
    cy.get ('.App-link').click ();
  });
});
